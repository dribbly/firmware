/*
	Firmware Main

	Contains the entrypoint for the firmware.
*/
#include "firmware.h"

// currentPosition & storedPosition are used to determine if the position has changed.
unsigned int currentPosition;
unsigned int storedPosition;

int main() {
	initialize();

	// set the initial position
	currentPosition = getPosition();
	storedPosition = currentPosition;
	setIntData(0x00, currentPosition);

	while(1) {

		// check for new position from encoder
		cli();
		currentPosition = getPosition();
		storedPosition = getIntData(0x00);
		if(currentPosition != storedPosition && hasNewSPIData() != true) {
			setIntData(0x00, currentPosition);
			storedPosition = currentPosition;
		}
		sei();

		// check for new position from SPI
		cli();
		currentPosition = getPosition();
		storedPosition = getIntData(0x00);
		if(currentPosition != storedPosition && hasNewSPIData() == true) {
			setPosition(storedPosition);
			currentPosition = storedPosition;
			processedSPIData();
		}
		sei();

	}

	return 0;
}

void initialize() {
	setupDebug();
	setupEncoder();
	setupSPI();

	sei();
}
