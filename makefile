# Dribbly Firmware Makefile

# Settings
MMCU=attiny2313
PART=t2313
F_CPU=20000000UL

OBJECTS=firmware.o lib/encoder.o lib/spi.o lib/debug.o
HEADERS=lib/types.h
OUTDIR=build/

PROGRAMMER=dragon_isp
PORT=usb

# Global
.PHONY: upload clean fuse

# AVR-GCC
GCC=avr-gcc
GCC_FLAGS=-Wall -O2 -g -mmcu=$(MMCU) -DF_CPU=$(F_CPU)
OBJCOPY=avr-objcopy
$(OUTDIR)firmware.hex: $(OUTDIR)firmware.elf
	$(OBJCOPY) -O ihex -j .text -j .data $< $@
$(OUTDIR)firmware.elf: $(addprefix $(OUTDIR),$(OBJECTS))
	$(GCC) $(GCC_FLAGS) -o $@ $(addprefix $(OUTDIR),$(OBJECTS))
$(addprefix $(OUTDIR),$(OBJECTS)): $(OUTDIR)%.o: %.c $(OBJECTS:.o=.h) $(HEADERS)
	mkdir -p $(dir $@)
	$(GCC) $(GCC_FLAGS) -c -o $@ $<

# AVRDUDE
AVRDUDE=avrdude
AVRDUDE_OPTS=-D -v -e -p $(PART) -c $(PROGRAMMER) -P $(PORT) -B 0.5

upload: $(OUTDIR)firmware.hex
	$(AVRDUDE) $(AVRDUDE_OPTS) -U flash:w:$(OUTDIR)firmware.hex:i

fuse:
	$(AVRDUDE) $(AVRDUDE_OPTS) -U lfuse:w:0xff:m -U hfuse:w:0xdf:m -U efuse:w:0xff:m

# Cleanup
clean:
	rm -rf $(OUTDIR)
