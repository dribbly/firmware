/*
	Firmware Header File

	Includes other header files and sets up global stuff.
*/
#ifndef __FIRMWARE_H__
#define __FIRMWARE_H__

#include <avr/io.h>
#include <avr/interrupt.h>
#include <stdio.h>

// Our libraries
#include "lib/encoder.h"
#include "lib/spi.h"
#include "lib/debug.h"

// initialize does setup for the entire system
void initialize();

#endif
