/*
	Encoder Library

	Handles the encoder events and keeps track of the current position.
	This library is set up with the following connections:

		A -> PD2, INT0, Arduino 2
		B -> PD3, INT1, Arduino 3
		Index -> PD4, Arduino 4

	Warning: including this library will attach an interrupt routine to INT0.
*/
#ifndef __LIB_ENCODER_H__
#define __LIB_ENCODER_H__

#include <avr/io.h>
#include <avr/interrupt.h>
#include "types.h"

// setupEncoder prepares the ports that the encoder is connected to
void setupEncoder();

// getPosition returns the current position count
unsigned int getPosition();

// setPosition overrides the current position count
void setPosition(unsigned int newPosition);

#endif
