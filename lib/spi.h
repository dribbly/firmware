/*
	SPI Library

	This library uses the USI to act as a slave device in SPI communications.
	This library uses the USI counter overflow interrupt (USIOIE).
*/
#ifndef __LIB_SPI_H__
#define __LIB_SPI_H__

#include <avr/io.h>
#include <avr/interrupt.h>
#include "types.h"
#include "debug.h"

// setupSPI initializes the USI for SPI hardware and enables the interrupt.
void setupSPI();

// setData sets a piece of data in the data table.
void setData(byte address, byte data);
void setIntData(byte address, unsigned int data);

// getData gets a piece of data from the data table.
byte getData(byte address);
unsigned int getIntData(byte address);

// hasNewSPIData returns true if there is newly ready SPI data.
bool hasNewSPIData();

// processedSPIData sets the new data flag to false to indicate the new SPI data has been processed.
void processedSPIData();


// SPI states
#define SPI_STATE_IDLE 0
#define SPI_STATE_READING 1
#define SPI_STATE_WRITING 2

// SPI_DATA_TABLE_SIZE is the number of bytes allocated for the SPI data table.
#define SPI_DATA_TABLE_SIZE 2


// spi_command_t contains details about the data being sent on the bus.
typedef struct {
	byte address;
	byte count;
	byte cursor;
} spi_command_t;

#endif
