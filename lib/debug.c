#include "debug.h"

void setupDebug() {
	DDRD |= _BV(DDD6);
}

inline void debugOn() {
	PORTD |= _BV(PORTD6);
}

inline void debugOff() {
	PORTD &= ~_BV(PORTD6);
}