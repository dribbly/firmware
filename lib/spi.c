#include "spi.h"

// state is the current state of the SPI communications.
byte state = SPI_STATE_IDLE;

// isSelected is true of this is the selected slave, otherwise it is false.
bool isSelected = false;


// dataTable is the data that is available via SPI communication.
byte dataTable[SPI_DATA_TABLE_SIZE];

// dataTableBuffer is the buffered data table that is copied when SS goes low (when the device is selected).
byte dataTableBuffer[SPI_DATA_TABLE_SIZE];

// newSPIData is true if there is unprocessed SPI data.
bool newSPIData = false;


// currentCommand is the currently running SPI command.
spi_command_t currentCommand;


// On SPI Byte transferred.
ISR(USI_OVERFLOW_vect) {
	USISR |= _BV(USIOIF);

	if(isSelected == false) {
		state = SPI_STATE_IDLE;
		return;
	}

	byte data;
	switch(state) {

		// Read the SPI command
		case SPI_STATE_IDLE:
			data = USIDR;
			currentCommand.count = (data & 0x38) >> 3;
			currentCommand.address = data & 0x07;
			currentCommand.cursor = currentCommand.address;

			if(currentCommand.count > 0) {
				if(data & 0x80) {
					state = SPI_STATE_WRITING;
				} else {
					state = SPI_STATE_READING;
					USIDR = dataTableBuffer[currentCommand.cursor++];
				}
			}

			break;

		// Send the next byte in the buffer.
		case SPI_STATE_READING:
			if(currentCommand.cursor == currentCommand.address + currentCommand.count) {
				state = SPI_STATE_IDLE;
			} else {
				USIDR = dataTableBuffer[currentCommand.cursor++];
			}
			break;

		// Save the next byte in the buffer.
		case SPI_STATE_WRITING:
			if(currentCommand.cursor == currentCommand.address + currentCommand.count) {
				state = SPI_STATE_IDLE;
				for(byte i=currentCommand.address; i<currentCommand.address + currentCommand.count; i++) {
					dataTable[i] = dataTableBuffer[i];
				}
				newSPIData = true;
			} else {
				dataTableBuffer[currentCommand.cursor++] = USIDR;
			}
			break;

		// In case the state was corrupted.
		default:
			state = SPI_STATE_IDLE;
			break;

	}
}

// PCINT interrupt detects if we are the selected slave.
ISR(PCINT_vect) {

	// SS = High
	if((PINB & _BV(DDB4)) > 0) {
		state = SPI_STATE_IDLE;
		isSelected = false;
	}

	// SS = Low
	else {
		isSelected = true;
		USISR &= 0xF0;
		for(byte i = 0; i<SPI_DATA_TABLE_SIZE; i++) {
			dataTableBuffer[i] = dataTable[i];
		}
	}

}

void setupSPI() {
	USICR = _BV(USIOIE) | _BV(USIWM0) | _BV(USICS1);
	PCMSK |= _BV(PCINT4);
	GIMSK |= _BV(PCIE);

	DDRB |= _BV(DDB6);
	DDRB &= (~_BV(DDB7)) & (~_BV(DDB5)) & (~_BV(DDB4));
}

inline void setData(byte address, byte data) {
	cli();
	dataTable[address] = data;
	sei();
}

inline void setIntData(byte address, unsigned int data) {
	cli();
	dataTable[address+1] = data;
	dataTable[address] = data >> 8;
	sei();
}

inline byte getData(byte address) {
	byte data;
	cli();
	data = dataTable[address];
	sei();
	return data;
}
inline unsigned int getIntData(byte address) {
	unsigned int data;
	cli();
	data = (dataTable[address] << 8) | dataTable[address];
	sei();
	return data;
}

inline bool hasNewSPIData() {
	return newSPIData;
}
inline void processedSPIData() {
	newSPIData = false;
}
