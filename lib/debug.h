/*
	Debug Library

	This library provides debug functionality.
*/
#ifndef __LIB_DEBUG_H__
#define __LIB_DEBUG_H__

#include <avr/io.h>

// setupDebug initializes the debug hardware.
void setupDebug();

// debugOn sets the debug signal high.
void debugOn();

// debugOff sets the debug signal low.
void debugOff();

#endif