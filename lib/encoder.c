#include "encoder.h"

// position stores the position as modified by the encoder interrupts.
unsigned int position = 256;

// "A" change interrupt
ISR(INT0_vect) {
	byte maskedIn = PIND & ( (1<<PIND2) | (1<<PIND3) );
	if(maskedIn == ( (1<<PIND2) | (1<<PIND3) ) ) {
		// Clockwise (A = high, B = high)
		position++;
	} else {
		// Counter-Clockwise (A = high, B = low)
		position--;
	}
}

unsigned int getPosition() {
	cli();
	unsigned int readPosition = position;
	sei();
	return readPosition;
}

void setPosition(unsigned int newPosition) {
	cli();
	position = newPosition;
	sei();
}

void setupEncoder() {

	// Enable pull up resistors
	DDRD &= ~_BV(DDD2) & ~_BV(DDD3) & ~_BV(DDD4);

	// Set up interrupts for rising edge on INT0 (D2)
	MCUCR |= _BV(ISC01)|_BV(ISC00);
	GIMSK |= _BV(INT0);

}
