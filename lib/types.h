/*
	Firmware Types

	Custom types used in the firmware.
*/
#ifndef __LIB_TYPES_H__
#define __LIB_TYPES_H__

typedef unsigned char byte;

typedef enum {
	false,
	true
} bool;

#endif
